package errors

import (
	"fmt"

	"github.com/pkg/errors"
)

//ServerError is the object to store a server error
type ServerError struct {
	message string
	code    int
}

func (err *ServerError) Error() string {
	return err.message
}

// WrapError wraps around pkg/errors and also provides easy mapping with the ErrorType without need for the ErrorStrings map.
func WrapError(errToWrap error, message string, i ...interface{}) error {
	return errors.Wrap(errToWrap, Format(message, i...))
}

// Format will return a formatted string based on the message.
func Format(message string, i ...interface{}) string {
	return fmt.Sprintf(message, i...)
}

// FormatError will return a formatted error based on the message.
func FormatError(message string, i ...interface{}) error {
	return fmt.Errorf(message, i...)
}
